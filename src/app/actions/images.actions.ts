import { Action } from '@ngrx/store';
import { Image } from '../models/images';

export enum ImagesActionTypes {
  LoadImages = '[Images] Load Images',
  ImagesLoaded = '[Images] Images Loaded',
}

export class LoadImages implements Action {
  readonly type = ImagesActionTypes.LoadImages;
}

export class ImagesLoaded implements Action {
  readonly type = ImagesActionTypes.ImagesLoaded;

  constructor(public payload: Image[]) {}
}

export type ImagesActions = 
  | LoadImages
  | ImagesLoaded
  ;
