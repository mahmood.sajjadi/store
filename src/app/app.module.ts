import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { NgModule, ApplicationRef } from '@angular/core';
import { RouterModule } from '@angular/router';
import { StoreModule, Store } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { EffectsModule } from '@ngrx/effects';
import {
  StoreRouterConnectingModule,
  RouterStateSerializer,
} from '@ngrx/router-store';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { createNewHosts, createInputTransfer, removeNgStyles } from '@angularclass/hmr';

import { MaterialModule } from './material/material.module';
import { AppComponent } from './app.component';
import { reducers, State } from './reducers';
import { environment } from '../environments/environment';
import { ImageEffects } from './effects/images.effects';
import { routes } from './routes';
import { HomeComponent } from './home/home.component';
import { ProfileComponent } from './profile/profile.component';
import { metaReducers, SET_ROOT_STATE } from './reducers/hmr.reducer';
import { AuthModule } from './auth/auth.module';
import { take } from 'rxjs/operators';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    ProfileComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    RouterModule.forRoot(routes),
    StoreModule.forRoot(reducers, { metaReducers }),
    StoreRouterConnectingModule.forRoot({ stateKey: 'router', }),
    !environment.production ? StoreDevtoolsModule.instrument({
      name: 'store',
      maxAge: 20
    }) : [],
    EffectsModule.forRoot([ImageEffects]),
    FormsModule,
    ReactiveFormsModule,
    MaterialModule,
    AuthModule.forRoot(),
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor(
    private appRef: ApplicationRef,
    private store: Store<State>
  ) { }

  public hmrOnInit(store) {
    if (!store || !store.state) {
      return;
    }
    // restore state
    this.store.dispatch({ type: SET_ROOT_STATE, payload: store.state });
    // restore input values
    if ('restoreInputValues' in store) {
      const restoreInputValues = store.restoreInputValues;
      // this isn't clean but gets the job done in development
      setTimeout(restoreInputValues);
    }
    this.appRef.tick();
    Object.keys(store).forEach(prop => delete store[prop]);
  }

  public hmrOnDestroy(store) {
    const cmpLocation = this.appRef.components.map(
      cmp => cmp.location.nativeElement
    );
    let currentState: State;
    this.store.pipe(
      take(1)
    ).subscribe(state => (currentState = state));
    store.state = currentState;
    // recreate elements
    store.disposeOldHosts = createNewHosts(cmpLocation);
    // save input values
    store.restoreInputValues = createInputTransfer();
    // remove styles
    removeNgStyles();
  }

  public hmrAfterDestroy(store) {
    // display new elements
    store.disposeOldHosts();
    delete store.disposeOldHosts;
  }
}
