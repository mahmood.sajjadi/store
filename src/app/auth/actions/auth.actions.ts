import { Action } from '@ngrx/store';
import { User, Authenticate } from '../models/user';

export enum AuthActionTypes {
  DoLogin = '[Auth] Do Login',
  DoLogout = '[Auth] Do Logout',
  LoginSuccess = '[Auth] Login Success',
  LoginFailure = '[Auth] Login Failure',
  LoginRedirect = '[Auth] Login Redirect',
}

export class DoLogin implements Action {
  readonly type = AuthActionTypes.DoLogin;

  constructor(public payload: Authenticate) {}
}

export class LoginSuccess implements Action {
  readonly type = AuthActionTypes.LoginSuccess;

  constructor(public payload: { user: User }) {}
}

export class LoginFailure implements Action {
  readonly type = AuthActionTypes.LoginFailure;

  constructor(public payload: any) {}
}

export class LoginRedirect implements Action {
  readonly type = AuthActionTypes.LoginRedirect;
}

export class DoLogout implements Action {
  readonly type = AuthActionTypes.DoLogout;
}

export type AuthActions =
  | DoLogin
  | LoginSuccess
  | LoginFailure
  | LoginRedirect
  | DoLogout;
