export interface Authenticate {
    username: string;
    password: string;
}

export interface User {
    name: string;
}

export interface Auth {
    success: boolean,
    name: string,
    error?: string,
}