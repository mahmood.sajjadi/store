import { Injectable } from '@angular/core';
import { Auth, User, Authenticate } from '../models/user';
import { Observable, of, throwError } from 'rxjs';
import { map } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  constructor(private readonly http: HttpClient) {}

  login({ username, password }: Authenticate): Observable<User> {
     return this.http.get<Auth>('api/auth.json').pipe(
       map(res => {
         if(res.success) {
           return { name: res.name }
         } else {
          throwError('Invalid username or password');
         }
       }, 
       err => throwError('Invalid username or password')
      )
     )
  }

  logout() {
    return of(true);
  }
}
