import { TestBed, inject } from '@angular/core/testing';
import { provideMockActions } from '@ngrx/effects/testing';
import { Observable } from 'rxjs';

import { ImagesEffects } from './images.effects';

describe('ImagesService', () => {
  let actions$: Observable<any>;
  let effects: ImagesEffects;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        ImagesEffects,
        provideMockActions(() => actions$)
      ]
    });

    effects = TestBed.get(ImagesEffects);
  });

  it('should be created', () => {
    expect(effects).toBeTruthy();
  });
});
