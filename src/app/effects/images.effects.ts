import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Effect, Actions, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import { tap, map, exhaustMap, catchError } from 'rxjs/operators';

import { PicturesService } from '../services/pictures.service';
import {
  LoadImages,
  ImagesLoaded,
  ImagesActionTypes,
} from '../actions/images.actions';
import { Image } from '../models/images';

@Injectable()
export class ImageEffects {
  @Effect()
  login$ = this.actions$.pipe(
    ofType(ImagesActionTypes.LoadImages),
    exhaustMap(() =>
      this.picturesService
        .getList()
        .pipe(
          map(images => new ImagesLoaded(images)),
        )
    )
  );

  constructor(
    private actions$: Actions,
    private picturesService: PicturesService,
    private router: Router
  ) {}
}
