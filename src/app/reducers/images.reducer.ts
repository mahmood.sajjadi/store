import { ImagesActions, ImagesActionTypes } from '../actions/images.actions';
import { Image } from '../models/images';

export interface State {
  Images: Image[],
  pending: boolean,
}

export const initialState: State = {
  Images: [],
  pending: true,
};

export function reducer(state = initialState, action: ImagesActions): State {
  switch (action.type) {
    case ImagesActionTypes.ImagesLoaded: {
      return {
        ...state,
        pending: false,
        Images: action.payload,
      };
    }

    default: {
      return state;
    }
  }
}

export const getImages = (state: State) => state.Images;
export const getPending = (state: State) => state.pending;