import {
  ActionReducer,
  ActionReducerMap,
  createFeatureSelector,
  createSelector,
  MetaReducer
} from '@ngrx/store';
import { environment } from '../../environments/environment';
import * as fromImages from './images.reducer';
import * as fromRoutes from './router.reducer';
import { RouterReducerState, routerReducer } from '@ngrx/router-store';

export interface State {
  Images: fromImages.State;
  router: RouterReducerState<fromRoutes.State>;
}


export const reducers: ActionReducerMap<State> = {
  Images: fromImages.reducer,
  router: routerReducer,
};

export const selectImagesState = createFeatureSelector<fromImages.State>('Images');

export const getImageList = createSelector(
  selectImagesState,
  fromImages.getImages
);

export const getImagesPending = createSelector(
  selectImagesState,
  fromImages.getPending
);
