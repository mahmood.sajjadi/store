import { Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { ProfileComponent } from './profile/profile.component';
import { AuthGuardService } from './auth/services/auth-guard.service';
import { PictureResolverService } from './services/picture-resolver.service';

export const routes: Routes = [
    { path: '', redirectTo: 'home', pathMatch: 'full' },
    { path: 'home', component: HomeComponent, resolve: { images: PictureResolverService } },
    { path: 'profile', component: ProfileComponent, canActivate: [AuthGuardService] },
];
