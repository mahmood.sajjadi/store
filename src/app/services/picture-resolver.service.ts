import { Injectable } from '@angular/core';
import { Resolve, RouterStateSnapshot, ActivatedRouteSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { Store, select } from '@ngrx/store';
import * as fromImages from '../reducers';
import { tap, switchMap, first, take } from 'rxjs/operators';
import { LoadImages } from '../actions/images.actions';
import { Image } from '../models/images';

@Injectable({
  providedIn: 'root'
})
export class PictureResolverService implements Resolve<Image[]> {
  pending$ = this.store.pipe(select(fromImages.getImagesPending));
  images$ = this.store.pipe(select(fromImages.getImageList));

  constructor(private store: Store<fromImages.State>) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Image[]> {
    return this.pending$.pipe(
      tap(x => {
        if (x) {
          this.store.dispatch(new LoadImages())
        }
      }),
      first(x => !x),
      switchMap(() => this.images$),
      take(1)
    );
  }
}
