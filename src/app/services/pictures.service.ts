import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Image } from '../models/images'

@Injectable({
  providedIn: 'root'
})
export class PicturesService {

  constructor(private readonly http: HttpClient) { }

  getList(): Observable<Image[]> {
    return this.http.get<Image[]>('api/images.json');
  }
}
